package com.ganna.ipmagixtask.base

import android.annotation.SuppressLint
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.paging.PagedList
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import com.ganna.ipmagixtask.R

abstract class BasePagedListAdapter(diffCallback: DiffUtil.ItemCallback<BaseEntity> = BaseDiffCallback())
    : PagedListAdapter<BaseEntity, androidx.recyclerview.widget.RecyclerView.ViewHolder>(diffCallback) {




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        return getViewHolder(parent, viewType)
    }

    override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {

        (holder as BaseViewHolder<*>).binding.root.setTag(R.string.position, position)

        bind(holder.binding, position)

    }


    open fun getViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder =
        BaseViewHolder(createBinding(parent, viewType))


    abstract fun createBinding(parent: ViewGroup, viewType: Int): ViewDataBinding


    protected abstract fun bind(binding: ViewDataBinding, position: Int)


    fun setList(list: PagedList<*>?) {
        submitList(list as PagedList<BaseEntity>?)
    }



    companion object {
        open class BaseDiffCallback : DiffUtil.ItemCallback<BaseEntity>() {
            override fun areItemsTheSame(oldItem: BaseEntity, newItem: BaseEntity) = oldItem.entityId() == newItem.entityId()


            @SuppressLint("DiffUtilEquals")
            override fun areContentsTheSame(oldItem: BaseEntity, newItem: BaseEntity) = oldItem == newItem
        }
    }
}