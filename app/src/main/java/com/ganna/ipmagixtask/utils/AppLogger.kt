package com.ganna.ipmagixtask.utils

import android.util.Log


object AppLogger {

    private const val TAG = "Logger"

    fun d(msg: String?) {
        msg?.let {
            Log.d(TAG,msg)
        }
    }

    fun e(e: Throwable?) {
        e?.printStackTrace()
    }
    

}