package com.ganna.ipmagixtask.utils

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.core.widget.NestedScrollView
import com.ganna.ipmagixtask.R

class CustomEmptyView(context: Context, attrs: AttributeSet?) : NestedScrollView(context, attrs) {


    private val titleView by lazy { findViewById<TextView>(R.id.tv_title) }
    private val subtitleView by lazy {  findViewById<TextView>(R.id.tv_subtitle) }
    private val imageView by lazy { findViewById<ImageView>(R.id.image_view) }
    private val buttonView by lazy { findViewById<Button>(R.id.btn_action) }


    var title :String? = null
        set(value) {
            field = value
            if (value ==null) titleView.visibility = View.GONE
            else {
                titleView.visibility = View.VISIBLE
                titleView.text = value
            }
        }

    var subTitle :String? = null
        set(value) {
            field = value
            if (value ==null) subtitleView.visibility = View.GONE
            else {
                subtitleView.visibility = View.VISIBLE
                subtitleView.text = value
            }
        }

    var buttonTxt :String? = null
        set(value) {
            field = value
            if (value ==null) buttonView.visibility = View.GONE
            else {
                buttonView.visibility = View.VISIBLE
                buttonView.text = value
            }
        }

     var imageDrawable :Drawable? = null
        set(value) {
            field = value
            if (value ==null) imageView.visibility = View.GONE
            else {
                imageView.visibility = View.VISIBLE
                imageView.setImageDrawable(imageDrawable)
            }
        }

    init {

        visibility = View.GONE

        LayoutInflater.from(context).inflate(R.layout.custom_view_list_empty, this, true)
        isFillViewport = true

        val a = context.theme.obtainStyledAttributes(attrs, R.styleable.CustomEmptyView, 0, 0)
        try {
            title = a.getString(R.styleable.CustomEmptyView_ev_title)
            subTitle = a.getString(R.styleable.CustomEmptyView_ev_subtitle)
            imageDrawable = a.getDrawable(R.styleable.CustomEmptyView_ev_image)
            buttonTxt = a.getString(R.styleable.CustomEmptyView_ev_action_button)
        } finally {
            a.recycle()
        }
    }


    fun setActionClickListener(onActionCLick : ()->Unit){
        buttonView.setOnClickListener { onActionCLick() }
    }

}