package com.ganna.ipmagixtask.utils

import com.blankj.utilcode.util.NetworkUtils
import java.util.regex.Pattern
import java.util.regex.PatternSyntaxException

open class NetworkHelper {

    fun isConnected()= NetworkUtils.isConnected()

    fun validIP(ipParam: String): Boolean {
        var ip = ipParam
        if (ip.isEmpty()) return false
        ip = ip.trim { it <= ' ' }
        return if ((ip.length < 6) and (ip.length > 15)) false else try {
            val pattern = Pattern.compile("^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$")
            val matcher = pattern.matcher(ip)
            matcher.matches()
        } catch (ex: PatternSyntaxException) {
            false
        }
    }


}