package com.ganna.ipmagixtask.utils

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import androidx.core.view.children
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.paging.PagedList
import androidx.recyclerview.widget.RecyclerView
import com.ganna.ipmagixtask.base.BaseEntity
import com.ganna.ipmagixtask.base.BasePagedListAdapter
import com.ganna.ipmagixtask.data.PaginationDataHolder
import com.ganna.ipmagixtask.databinding.LayoutPaginationBinding
import com.ganna.ipmagixtask.network.util.ResultError
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class PaginationLayout(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs),KoinComponent {

    private val networkHelper : NetworkHelper by inject()


    private val mStatusLayoutBinding by lazy { LayoutPaginationBinding.inflate(LayoutInflater.from(context),this,true) }
    private val mRecyclerView by lazy { children.elementAt(0) as RecyclerView }


    fun<T: BaseEntity> getBoundaryCallback() = object : PagedList.BoundaryCallback<T>() {

        override fun onZeroItemsLoaded() {
            super.onZeroItemsLoaded()
            showEmpty()
        }


        override fun onItemAtFrontLoaded(itemAtFront: T) {
            super.onItemAtFrontLoaded(itemAtFront)
            resetAllViews()
        }
    }

    var emptyConfiguration : EmptyConfiguration? = null
    set(value) {
        field = value
        value?.let {
            mStatusLayoutBinding.emptyView.title = emptyConfiguration?.title
            mStatusLayoutBinding.emptyView.subTitle = emptyConfiguration?.subTitle
            mStatusLayoutBinding.emptyView.buttonTxt = emptyConfiguration?.actionBtnTxt
            emptyConfiguration?.img?.let { mStatusLayoutBinding.emptyView.imageDrawable = ContextCompat.getDrawable(context,it) }
            mStatusLayoutBinding.emptyView.setActionClickListener { emptyConfiguration?.onActionClick?.invoke() }
        }
    }


    fun <T : BaseEntity> setUp(lifecycleOwner: LifecycleOwner, dataHolder: PaginationDataHolder<T, *>,
                               adapter: BasePagedListAdapter?, emptyConfiguration: EmptyConfiguration?
                               , onRetryClick: () -> Unit) {

        mRecyclerView.adapter = adapter

        this.emptyConfiguration = emptyConfiguration

        mStatusLayoutBinding.btnRetry.setOnClickListener {
            onRetryClick()
        }

        dataHolder.dataLiveData.observe(lifecycleOwner, Observer {
            adapter?.setList(it)
        })

        if (networkHelper.isConnected()){

            dataHolder.remoteApiStatusLiveData.observe(lifecycleOwner, Observer {
                it?.let {
                    when (it.status) {
                        PaginationDataHolder.ApiResultStatus.Status.ERROR -> {

                            hideLoading()

                            if (it.isFirstPage()) {
                                AppLogger.d("Total data error")
                                showError(it.error)
                            } else {
                                AppLogger.d("Current page error")
                            }
                        }
                        PaginationDataHolder.ApiResultStatus.Status.LOADING -> {

                            if (it.isFirstPage()) {
                                showInitialLoading()
                            } else {
                                showLoadingMore()
                            }
                        }
                        PaginationDataHolder.ApiResultStatus.Status.SUCCESS -> {
                            hideLoading()

                            if (it.isFirstPage() && (adapter?.itemCount == 0 || adapter == null)) {
                                showEmpty()
                            }
                        }
                    }
                }
            })

        }

    }

    private fun showError(error: ResultError?) {
        resetAllViews()

        mStatusLayoutBinding.tvError.text = error?.message
        mStatusLayoutBinding.errorView.visibility = View.VISIBLE
    }



    private fun showInitialLoading() {
        resetAllViews()
        mStatusLayoutBinding.loadingView.visibility = View.VISIBLE
    }

    private fun showLoadingMore() {
        resetAllViews()
        mStatusLayoutBinding.loadingViewBottom.visibility = View.VISIBLE
    }

    private fun hideLoading() {
        resetAllViews()
    }

    private fun showEmpty() {
        resetAllViews()
        mStatusLayoutBinding.emptyView.visibility = View.VISIBLE
    }


    private fun resetAllViews() {
        mStatusLayoutBinding.errorView.visibility = View.GONE
        mStatusLayoutBinding.loadingView.visibility = View.GONE
        mStatusLayoutBinding.loadingViewBottom.visibility = View.GONE
        mStatusLayoutBinding.emptyView.visibility = View.GONE
    }

    data class EmptyConfiguration(val title :String? = null , val subTitle: String? = null , val actionBtnTxt:String? = null ,val img:Int? = null, val onActionClick : (()->Unit)? = null)

}