package com.ganna.ipmagixtask.di


import com.ganna.ipmagixtask.utils.NetworkHelper
import org.koin.dsl.module


fun provideNetworkHelper() = NetworkHelper()

val appModule = module {
    single { provideNetworkHelper() }
}


