package com.ganna.ipmagixtask.di

import android.app.Application
import androidx.room.Room
import com.ganna.ipmagixtask.database.AppDataBase
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module


fun provideRoom(app: Application):AppDataBase {
    return Room.databaseBuilder(app, AppDataBase::class.java, "app_database")
            .fallbackToDestructiveMigration()
            .build()

}

val databaseModule = module {

    single { provideRoom(androidApplication()) }

    single { get<AppDataBase>().photosDao() }

}



