package com.ganna.ipmagixtask.di

import com.ganna.ipmagixtask.network.remoteDataSource.PhotosRemoteDataSource
import org.koin.dsl.module

val remoteDataSourceModule = module {

    factory { PhotosRemoteDataSource(get()) }

}