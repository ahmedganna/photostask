package com.ganna.ipmagixtask.di

import com.ganna.ipmagixtask.repository.PhotosRepository
import org.koin.dsl.module



val repositoryModule = module {

    factory { PhotosRepository(get(),get()) }

}


