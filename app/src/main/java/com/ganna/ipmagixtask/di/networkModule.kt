package com.ganna.ipmagixtask.di

import com.ganna.ipmagixtask.data.Constants
import com.ganna.ipmagixtask.network.api.PhotosApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory



fun provideOkHttpLoggingInterceptor() = HttpLoggingInterceptor().apply {
    level = HttpLoggingInterceptor.Level.BODY }



fun provideOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {

    val httpClientBuilder = OkHttpClient.Builder()

    httpClientBuilder.addInterceptor(httpLoggingInterceptor)
    return httpClientBuilder.build()
}


fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder()
            .baseUrl(Constants.BASE_API_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create()).build()
}

@JvmField
val networkModule = module {

    single { provideOkHttpLoggingInterceptor() }

    single{ provideOkHttpClient(get()) }

    single{ provideRetrofit(get())}

    factory { get<Retrofit>().create(PhotosApi::class.java) }


}



