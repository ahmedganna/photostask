package com.ganna.ipmagixtask.di


import com.ganna.ipmagixtask.features.main.viewModel.PhotosViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val viewModelModule = module {
    viewModel { PhotosViewModel(androidApplication(),get()) }

}