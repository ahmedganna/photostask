package com.ganna.ipmagixtask.features.main.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagedList
import com.ganna.ipmagixtask.data.PaginationDataHolder
import com.ganna.ipmagixtask.data.models.apiResponse.PhotoItem
import com.ganna.ipmagixtask.repository.PhotosRepository

class PhotosViewModel(app: Application, private val repository: PhotosRepository) : AndroidViewModel(app) {


    lateinit var paginationDataHolder : PaginationDataHolder<PhotoItem, *>


    fun getOrRefreshData(boundaryCallback: PagedList.BoundaryCallback<PhotoItem>) {
        if (!::paginationDataHolder.isInitialized) paginationDataHolder = repository.observeAllPhotos(viewModelScope,boundaryCallback)
        else paginationDataHolder.reload()
    }

}
