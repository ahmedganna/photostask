package com.ganna.ipmagixtask.features.main.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.ganna.ipmagixtask.R
import com.ganna.ipmagixtask.databinding.ActivityMainBinding
import com.ganna.ipmagixtask.features.main.viewModel.PhotosViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {


    private val mViewModel by viewModel<PhotosViewModel>()

    private val mAdapter = PhotosPagedAdapter()

    lateinit var mBinding : ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding =  DataBindingUtil.setContentView(this, R.layout.activity_main ) as ActivityMainBinding

        subscribeUi()
    }

    private fun subscribeUi() {

        mViewModel.getOrRefreshData(mBinding.plPhotos.getBoundaryCallback())

        mBinding.plPhotos.setUp(this,mViewModel.paginationDataHolder,mAdapter ,
            onRetryClick = {mViewModel.getOrRefreshData(mBinding.plPhotos.getBoundaryCallback()) } , emptyConfiguration = mBinding.plPhotos.emptyConfiguration)

    }

}
