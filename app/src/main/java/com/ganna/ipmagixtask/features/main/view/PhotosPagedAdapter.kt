package com.ganna.ipmagixtask.features.main.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.bumptech.glide.Glide
import com.ganna.ipmagixtask.R
import com.ganna.ipmagixtask.base.BasePagedListAdapter
import com.ganna.ipmagixtask.data.models.apiResponse.PhotoItem
import com.ganna.ipmagixtask.databinding.ItemPhotoBinding

class PhotosPagedAdapter : BasePagedListAdapter() {

    override fun createBinding(parent: ViewGroup, viewType: Int): ViewDataBinding {
        return DataBindingUtil.inflate(LayoutInflater.from(parent.context),  R.layout.item_photo, parent, false)
    }


    override fun bind(binding: ViewDataBinding, position: Int) {

        val item = getItem(position) as PhotoItem

        (binding as ItemPhotoBinding).apply {

            val context = binding.root.context
            when{
                item.isAd()-> {
                    Glide.with(context)
                        .load(item.adRes)
                        .into(imageView)
                }
                else -> {
                    Glide.with(context)
                        .load(item.fullUrl())
                        .into(imageView)
                }
            }

        }
    }

}