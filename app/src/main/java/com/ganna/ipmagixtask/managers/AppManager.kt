package com.ganna.ipmagixtask.managers

import androidx.multidex.MultiDexApplication
import com.ganna.ipmagixtask.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class AppManager : MultiDexApplication() {


    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    private fun initKoin() {
        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@AppManager)
            modules(
                appModule,
                databaseModule,
                networkModule,
                remoteDataSourceModule,
                viewModelModule,
                repositoryModule,
            )
        }
    }
}