package com.ganna.ipmagixtask.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.PagedList
import com.ganna.ipmagixtask.network.pagedDataSource.BasePagingDataSourceFactory
import com.ganna.ipmagixtask.network.util.ResultError

data class PaginationDataHolder<T,DS: BasePagingDataSourceFactory<*, *>>(val remoteDataSourceFactory: DS,
                                                                         val dataLiveData: LiveData<PagedList<T>>){

    val remoteApiStatusLiveData = Transformations.switchMap(remoteDataSourceFactory.dataSourceLiveData){
        it.apiResponseStatus
    }


    fun reload(){
        remoteDataSourceFactory.dataSourceLiveData.value?.invalidate()
    }

    data class ApiResultStatus(val status: Status, val error: ResultError? = null, val currentPage:Int ) {

        enum class Status {
            ERROR,
            LOADING,
            SUCCESS
        }

        companion object {

            fun  error(error: ResultError? = null, currentPage:Int): ApiResultStatus {
                return ApiResultStatus(Status.ERROR,  error,currentPage = currentPage)
            }

            fun  loading( currentPage:Int): ApiResultStatus {
                return ApiResultStatus(Status.LOADING,currentPage = currentPage)
            }

            fun success( currentPage:Int) =  ApiResultStatus(Status.SUCCESS,currentPage = currentPage)
        }

        fun isFirstPage() =  currentPage ==0

    }
}