package com.ganna.ipmagixtask.data.models.apiResponse

import com.google.gson.annotations.SerializedName

open  class BaseResponse(
    @field:SerializedName("stat")
    val stat: String? = null
) {
    fun isSuccess() = stat == "ok"
}
