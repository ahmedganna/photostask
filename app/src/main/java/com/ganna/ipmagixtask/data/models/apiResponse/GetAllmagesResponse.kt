package com.ganna.ipmagixtask.data.models.apiResponse

import androidx.annotation.DrawableRes
import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.ganna.ipmagixtask.base.BaseEntity
import com.google.gson.annotations.SerializedName
import org.jetbrains.annotations.NotNull

data class GetAllImagesResponse(

	@field:SerializedName("photos")
	val photosNode: PhotosNode? = null

):BaseResponse()

data class PhotosNode(

	@field:SerializedName("perpage")
	val perpage: Int? = null,

	@field:SerializedName("total")
	val total: Int? = null,

	@field:SerializedName("pages")
	val pages: Int? = null,

	@field:SerializedName("photo")
	val photos: List<PhotoItem>? = null,

	@field:SerializedName("page")
	val page: Int? = null
)

@Entity(tableName = "photos")
data class PhotoItem (

	@field:SerializedName("owner")
	var owner: String? = null,

	@field:SerializedName("server")
	var server: String? = null,

	@field:SerializedName("ispublic")
	var ispublic: Int? = null,

	@field:SerializedName("isfriend")
	var isfriend: Int? = null,

	@field:SerializedName("farm")
	var farm: Int? = null,

	@PrimaryKey
	@field:SerializedName("id")
	var id: String = "",

	@field:SerializedName("secret")
	var secret: String? = null,

	@field:SerializedName("title")
	var title: String? = null,

	@field:SerializedName("isfamily")
	var isfamily: Int? = null,

	@Ignore
	@DrawableRes var adRes: Int? = null

):BaseEntity{

	override fun entityId() = id.toInt()

    fun fullUrl() = "https://live.staticflickr.com/$server/${id}_${secret}_b.jpg"

	fun isAd() = adRes != null
}

