package com.ganna.ipmagixtask.database.dao

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import com.ganna.ipmagixtask.data.models.apiResponse.PhotoItem

@Dao
interface PhotosDao : BaseDao<PhotoItem>{


    @Query("SELECT * FROM photos ")
    fun getAll(): DataSource.Factory<Int, PhotoItem>


    @Query("DELETE From photos")
    suspend fun removeAll()


    @Transaction
    suspend fun removeAndInsertAll(newData:List<PhotoItem>){
        removeAll()
        insertAll(newData)
    }

}