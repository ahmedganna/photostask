package com.ganna.ipmagixtask.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.ganna.ipmagixtask.data.models.apiResponse.PhotoItem
import com.ganna.ipmagixtask.database.dao.PhotosDao


@Database(entities = [PhotoItem::class] ,version = 5)
abstract class AppDataBase : RoomDatabase() {

    abstract fun photosDao(): PhotosDao

}