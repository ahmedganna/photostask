package com.ganna.ipmagixtask.network.remoteDataSource

import com.ganna.ipmagixtask.data.models.apiResponse.BaseResponse
import com.ganna.ipmagixtask.network.util.ApiErrorHandler
import com.ganna.ipmagixtask.network.util.DataResult
import com.ganna.ipmagixtask.network.util.ResultError
import com.ganna.ipmagixtask.utils.AppLogger
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.koin.core.component.KoinComponent
import retrofit2.Response


abstract class BaseRemoteDataSource : KoinComponent {



    suspend fun <B : BaseResponse> proceedRequest(apiCall: suspend () -> Response<B>): DataResult<B>{
        try {
            val response = apiCall()
            if (response.isSuccessful) {
                val body = response.body()

                body?.let {
                    return if (it.isSuccess()) return DataResult.success(it)
                    else DataResult.error(ResultError(it.stat))
                }

            }
            val httpMessage = "${response.code()} ${response.message()}"

            return error(
                httpMessage,
                response.code(),
                withContext(Dispatchers.IO) { response.errorBody()?.string() })

        } catch (e: Exception) {
            e.printStackTrace()
            return error(ApiErrorHandler.getNetworkExceptionMessage(e))
        }
    }


    private fun <T> error(
        message: String? = null,
        httpCode: Int? = null,
        errorBody: String? = null
    ): DataResult<T> {
        AppLogger.e(Exception(message))
        return DataResult.error(ResultError(message, httpCode, errorBody))
    }

}

