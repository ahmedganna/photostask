package com.ganna.ipmagixtask.network.util

/**
 * A generic class that holds a value with its loading status.
 *
 * Result is usually created by the Repository classes where they return
 * `LiveData<Result<T>>` to pass back the latest data to the UI with its fetch status.
 */

data class DataResult<out T>(val status: Status, val data: T?, val error: ResultError?) {

    enum class Status {
        SUCCESS,
        ERROR,
        LOADING
    }

    companion object {
        fun <T> success(data: T): DataResult<T> {
            return DataResult(Status.SUCCESS, data, null)
        }

        fun <T> error(error: ResultError? = null, data: T? = null): DataResult<T> {
            return DataResult(Status.ERROR, data, error)
        }

        fun <T> loading(data: T? = null): DataResult<T> {
            return DataResult(Status.LOADING, data, null)
        }
    }

}

class ResultError (val message: String?, val httpCode : Int?=null, val errorBody : String?=null)

data class ResultContainer<T>(val dataResult: DataResult<T>,val responseRaw:String? = null)