package com.ganna.ipmagixtask.network.pagedDataSource

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.PagedList

abstract class BasePagingDataSourceFactory<T,ST:BasePagedDataSource<T>> : DataSource.Factory<Int, T>() {


    val dataSourceLiveData = MutableLiveData<ST>()


    override fun create(): BasePagedDataSource<T> {
        val dataSource = getDataSource()
        dataSourceLiveData.postValue(dataSource)
        return dataSource
    }

    abstract fun getDataSource():ST

    companion object {
        private const val PAGE_SIZE = 10

        fun pagedListConfig() = PagedList.Config.Builder()
                .setInitialLoadSizeHint(PAGE_SIZE)
                .setPageSize(PAGE_SIZE)
                .build()
    }

}