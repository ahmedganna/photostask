package com.ganna.ipmagixtask.network.pagedDataSource

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.blankj.utilcode.util.StringUtils
import com.ganna.ipmagixtask.R
import com.ganna.ipmagixtask.data.PaginationDataHolder
import com.ganna.ipmagixtask.network.util.DataResult
import com.ganna.ipmagixtask.network.util.ResultError
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch


abstract class BasePagedDataSource<T>(private val scope: CoroutineScope): PageKeyedDataSource<Int, T>() {


    private var currentPage  = 0

    var apiResponseStatus: MutableLiveData<PaginationDataHolder.ApiResultStatus> = MutableLiveData()


    private fun updateState(result: PaginationDataHolder.ApiResultStatus) {
        this.apiResponseStatus.postValue(result)
    }

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, T>) {
        currentPage =  0
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, T>) {

    }


    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, T>) {
        currentPage =  params.key
    }



    protected fun <A> fetchData(
        networkCall: suspend () -> DataResult<A>,
        saveCallResult: suspend (A) -> Unit,
        callback: (A) -> Unit) {


        scope.launch(getJobErrorHandler()) {
            updateState(PaginationDataHolder.ApiResultStatus.loading(currentPage))

            val response = networkCall.invoke()
            if (response.status == DataResult.Status.SUCCESS) {

                val result = response.data

                if (result!=null){

                    callback(result)

                    updateState(PaginationDataHolder.ApiResultStatus.success(currentPage))

                    saveCallResult(result)

                }else{
                    postError(ResultError(StringUtils.getString(R.string.no_data_available)))
                }

            } else if (response.status == DataResult.Status.ERROR) {
                postError(response.error)
            }
        }

    }


    private fun getJobErrorHandler() = CoroutineExceptionHandler { _, e ->
        postError(ResultError(e.message
                ?: e.toString()))
    }

    private fun postError(error: ResultError?) {
        updateState(PaginationDataHolder.ApiResultStatus.error(error,currentPage))
    }


}