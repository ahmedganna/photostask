package com.ganna.ipmagixtask.network.util

import com.blankj.utilcode.util.StringUtils
import com.ganna.ipmagixtask.R
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException

object ApiErrorHandler {


    fun getNetworkExceptionMessage(e:Throwable): String {
        e.printStackTrace()

        return when(e){
            is HttpException -> StringUtils.getString(R.string.server_error)
            is SocketTimeoutException -> StringUtils.getString( R.string.timeout)
            is IOException -> e.message
            else -> null
        } ?: e.toString()

    }
}