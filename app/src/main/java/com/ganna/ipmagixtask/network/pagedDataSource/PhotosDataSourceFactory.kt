package com.ganna.ipmagixtask.network.pagedDataSource


import com.ganna.ipmagixtask.R
import com.ganna.ipmagixtask.data.models.apiResponse.PhotoItem
import com.ganna.ipmagixtask.database.dao.PhotosDao
import com.ganna.ipmagixtask.network.remoteDataSource.PhotosRemoteDataSource
import kotlinx.coroutines.CoroutineScope

class PhotosDataSourceFactory(
    private val dataSource: PhotosRemoteDataSource,
    private val dao: PhotosDao,
    private val scope: CoroutineScope) : BasePagingDataSourceFactory<PhotoItem,PhotosPageDataSource>() {


    override fun getDataSource(): PhotosPageDataSource = PhotosPageDataSource(dataSource,dao, scope)

}
class PhotosPageDataSource(private val dataSource: PhotosRemoteDataSource, private val dao: PhotosDao, scope: CoroutineScope) : BasePagedDataSource<PhotoItem>(scope) {

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, PhotoItem>) {
        super.loadInitial(params, callback)
        get(1, params.requestedLoadSize) {
            callback.onResult(it, null, 2)
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, PhotoItem>) {
        super.loadAfter(params, callback)
        val page = params.key
        get(page, params.requestedLoadSize) {
            callback.onResult(it, page + 1)
        }
    }

    private fun get(page: Int, pageSize: Int, onLoad: (List<PhotoItem>) -> Unit) {
        fetchData(
                {dataSource.getPhotos(page,pageSize)},
                {
                    it.photosNode?.photos?.let {
                      dao.insertAll(it)
                    }
                },
                {
                    val data : MutableList<PhotoItem> = it.photosNode?.photos?.toMutableList()?: ArrayList()

                    for (i in 1 .. data.size){
                        val div = i / 5
                        val mod = i % 5
                        if ( mod == 0 ) data.add((i+div) -1,PhotoItem(adRes = R.drawable.ad_mob))
                    }

                    onLoad(data)
                }
        )
    }



}
