package com.ganna.ipmagixtask.network.remoteDataSource

import com.ganna.ipmagixtask.network.api.PhotosApi


class PhotosRemoteDataSource (private val api: PhotosApi) : BaseRemoteDataSource() {


    suspend fun getPhotos(page: Int, pageSize: Int)  = proceedRequest { api.getAllImages(page,pageSize) }

}