package com.ganna.ipmagixtask.network.api

import com.ganna.ipmagixtask.data.Constants
import com.ganna.ipmagixtask.data.models.apiResponse.GetAllImagesResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query


interface PhotosApi {



    @GET("?method=flickr.photos.search&format=json&nojsoncallback=50&text=Color&api_key=${Constants.FLICKER_API_KEY}")
    suspend fun getAllImages(
            @Query("page") page: Int,
            @Query("per_page") pageSize: Int
    ):Response<GetAllImagesResponse>


}