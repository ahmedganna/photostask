package com.ganna.ipmagixtask.repository


import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.ganna.ipmagixtask.data.PaginationDataHolder
import com.ganna.ipmagixtask.data.models.apiResponse.PhotoItem
import com.ganna.ipmagixtask.database.dao.PhotosDao
import com.ganna.ipmagixtask.network.pagedDataSource.BasePagingDataSourceFactory
import com.ganna.ipmagixtask.network.pagedDataSource.PhotosDataSourceFactory
import com.ganna.ipmagixtask.network.remoteDataSource.PhotosRemoteDataSource
import kotlinx.coroutines.CoroutineScope


class PhotosRepository(private val remoteDataSource: PhotosRemoteDataSource, private val dao: PhotosDao):BaseRepository() {


    fun observeAllPhotos( scope: CoroutineScope, boundaryCallback: PagedList.BoundaryCallback<PhotoItem>):
            PaginationDataHolder<PhotoItem, PhotosDataSourceFactory> {

        val remoteDataSourceFactory = PhotosDataSourceFactory(remoteDataSource,dao, scope)


        val remoteLiveData = LivePagedListBuilder(remoteDataSourceFactory, BasePagingDataSourceFactory.pagedListConfig()).build()

        val localLiveData = LivePagedListBuilder(dao.getAll(),BasePagingDataSourceFactory.pagedListConfig()).setBoundaryCallback(boundaryCallback).build()

        val dataLiveData = getAppropriatePagedLiveData(localLiveData,remoteLiveData)

        return PaginationDataHolder(remoteDataSourceFactory, dataLiveData)
    }



}
