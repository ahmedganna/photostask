package com.ganna.ipmagixtask.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.paging.PagedList
import com.blankj.utilcode.util.StringUtils
import com.ganna.ipmagixtask.R
import com.ganna.ipmagixtask.network.util.DataResult
import com.ganna.ipmagixtask.network.util.ResultError
import com.ganna.ipmagixtask.utils.NetworkHelper
import kotlinx.coroutines.Dispatchers
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

open class BaseRepository :KoinComponent{

    val networkHelper : NetworkHelper by inject()



    fun <T, A> networkWithCacheLiveData(databaseQuery: suspend () -> T?,
                                        networkCall: suspend () -> DataResult<A>,
                                        saveCallResult: suspend (A) -> Unit): LiveData<DataResult<T>> =
            liveData(Dispatchers.IO) {

                try {

                    //Emit loading
                    emit(DataResult.loading())

                    val dbData = databaseQuery.invoke()

                    val isEmptyList = ( dbData is List<*>  && dbData.isEmpty() )

                    val isNetworkAvailable = networkHelper.isConnected()

                    if (dbData==null || isEmptyList ) { // No local data available

                        if (isNetworkAvailable){

                            // Network call
                            val response = networkCall.invoke()

                            // Network Success
                            if (response.status == DataResult.Status.SUCCESS) {

                                // Save to local
                                response.data?.let { saveCallResult(it) }

                                //Emit from local
                                emit(DataResult.success(databaseQuery.invoke()!!))


                            }else if (response.status == DataResult.Status.ERROR) {

                                //Emit response error
                                emit(DataResult.error(response.error))
                            }

                        }else{

                            //Emit network error
                            emit(DataResult.error(ResultError(message = StringUtils.getString(R.string.no_internet_connection_msg)), null))
                        }


                    }else{ //Local data available

                        //Emit from local
                        emit(DataResult.success(dbData))

                        if (isNetworkAvailable){ // Check internet

                            // Network call
                            val response = networkCall.invoke()

                            // Network Success
                            if (response.status == DataResult.Status.SUCCESS) {

                                // Save to local
                                response.data?.let { saveCallResult(it) }

                                //Emit from local
                                emit(DataResult.success(databaseQuery.invoke()!!))
                            }
                        }
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                    emit(DataResult.error<T>(ResultError(e.message)))
                }

            }


     fun<T> getAppropriatePagedLiveData(localLiveData: LiveData<PagedList<T>>, remoteLiveData: LiveData<PagedList<T>>):  LiveData<PagedList<T>> {
        return if (networkHelper.isConnected()) remoteLiveData else localLiveData
    }


}